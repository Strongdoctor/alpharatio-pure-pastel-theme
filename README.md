# Pure Pastel Theme
This is a <a href="http://compass-style.org/">Compass</a> project.

The SASS code is extremely versatile; it's incredibly easy to change colors and/or fonts.